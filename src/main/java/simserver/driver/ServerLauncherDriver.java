package simserver.driver;

import simserver.server.SimServerLauncher;

public class ServerLauncherDriver {

	public static void main(String[] args) {
		try{
			new SimServerLauncher().launchServer();
		} catch(Exception ex){
			System.out.println(ex.getStackTrace());
		}
	}

}