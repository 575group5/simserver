package simserver.history.conversation;

import java.util.Collections;
import java.util.List;

import simcommon.conversation.Conversation;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;

public class NoHistoryHistoryKeeper implements IConversationHistoryKeeper {

	private static IConversationHistory EMPTY_CONVESATION_HISTORY = new IConversationHistory() {
		@Override
		public List<MessageEntry> getHistory() {
			return Collections.emptyList();
		}
	};

	@Override
	public void recordMessageSent(Conversation conversation, IUserMessageOrigin source, IUserMessage userMessage) {
		// do nothing
	}

	@Override
	public IConversationHistory getConversationHistory(Conversation conversation) {
		return EMPTY_CONVESATION_HISTORY;
	}

}

