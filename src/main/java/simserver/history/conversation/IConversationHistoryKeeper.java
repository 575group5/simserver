package simserver.history.conversation;

import simcommon.conversation.Conversation;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;

public interface IConversationHistoryKeeper {

	void recordMessageSent(Conversation conversation, IUserMessageOrigin source, IUserMessage userMessage);
	IConversationHistory getConversationHistory(Conversation conversation);
}
