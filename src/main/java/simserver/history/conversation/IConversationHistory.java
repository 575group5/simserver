package simserver.history.conversation;

import java.time.Instant;
import java.util.List;

import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;

public interface IConversationHistory {

	public List<MessageEntry> getHistory();

	public interface MessageEntry {
		public Instant getInstant();

		public IUserMessageOrigin getMessageOrigin();

		public IUserMessage getMessage();
	}
}
