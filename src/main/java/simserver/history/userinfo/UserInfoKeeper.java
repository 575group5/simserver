package simserver.history.userinfo;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.utilities.ParamUtil;

public abstract class UserInfoKeeper {

	protected Collection<IUserInfoChangeListener> mChangeListeners = new CopyOnWriteArrayList<>();

	public void addListener(IUserInfoChangeListener listener) {
		ParamUtil.validateParamNotNull("listener must be nonnull", listener);
		mChangeListeners.add(listener);
	}

	public void removeListener(IUserInfoChangeListener listener) {
		ParamUtil.validateParamNotNull("listener must be nonnull", listener);
		mChangeListeners.remove(listener);
	}

	public abstract Collection<User> getAllUsers();

	public abstract User getUserFromId(UserId userId);

	public abstract boolean isUserActive(UserId userId);

	public abstract User addUser(UserInfo userInfo);

	public abstract void removeUser(UserId userId);

	public abstract void markUserActive(UserId userId);

	public abstract void markUserInactive(UserId userId);

	protected void notifyListenersUserAdded(User addedUser) {
		for (IUserInfoChangeListener listener : mChangeListeners) {
			listener.onUserAdded(addedUser);
		}
	}

	protected void notifyListenersUserRemoved(User removedUser) {
		for (IUserInfoChangeListener listener : mChangeListeners) {
			listener.onUserRemoved(removedUser);
		}
	}
}
