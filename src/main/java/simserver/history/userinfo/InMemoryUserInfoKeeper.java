package simserver.history.userinfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import simcommon.entities.IUserIdGenerator;
import simcommon.entities.InMemoryUserIdGenerator;
import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.utilities.ParamUtil;

public class InMemoryUserInfoKeeper extends UserInfoKeeper {

	private Map<UserId, Pair<User, Boolean>> mUserData = new HashMap<>();
	private IUserIdGenerator mUserIdGenerator;
	
	public InMemoryUserInfoKeeper() {
		this(new InMemoryUserIdGenerator());
	}

	// for testing
	/* package */ InMemoryUserInfoKeeper(IUserIdGenerator userIdGenerator) {
		mUserIdGenerator = userIdGenerator;
	}

	@Override
	/**
	 * @return User with given UserId, or null if the UserId is invalid (not
	 *         associated with any user)
	 */
	public User getUserFromId(UserId userId) {
		ParamUtil.validateParamNotNull("must specify user id", userId);
		if (mUserData.containsKey(userId)) {
			return mUserData.get(userId).getLeft();
		}
		return null;
	}

	@Override
	public boolean isUserActive(UserId userId) {
		ParamUtil.validateParamNotNull("must specify user id", userId);
		ParamUtil.validateConditionTrue("invalid user id", mUserData.containsKey(userId));

		return mUserData.get(userId).getRight();
	}

	@Override
	public User addUser(UserInfo userInfo) {
		ParamUtil.validateParamNotNull("must specify user info", userInfo);

		UserId id = mUserIdGenerator.generateUserId();
		User user = new User(userInfo, id);
		mUserData.put(id, Pair.of(user, true));
		notifyListenersUserAdded(user);
		return user;
	}

	@Override
	public void removeUser(UserId userId) {
		ParamUtil.validateParamNotNull("must specify user id", userId);
		ParamUtil.validateConditionTrue("invalid user id", mUserData.containsKey(userId));

		User removed = mUserData.remove(userId).getLeft();
		notifyListenersUserRemoved(removed);
	}

	@Override
	public void markUserActive(UserId userId) {
		ParamUtil.validateParamNotNull("must specify user id", userId);
		ParamUtil.validateConditionTrue("invalid user id", mUserData.containsKey(userId));

		mUserData.put(userId, Pair.of(getUserFromId(userId), true));
	}

	@Override
	public void markUserInactive(UserId userId) {
		ParamUtil.validateParamNotNull("must specify user id", userId);
		ParamUtil.validateConditionTrue("invalid user id", mUserData.containsKey(userId));

		mUserData.put(userId, Pair.of(getUserFromId(userId), false));
	}

	@Override
	public Collection<User> getAllUsers() {
		Collection<User> users = new ArrayList<>();
		for (Pair<User, Boolean> entry : mUserData.values()) {
			users.add(entry.getLeft());
		}
		return users;
	}
}
