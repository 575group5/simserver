package simserver.history.userinfo;

import java.util.Collection;

import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationId;
import simcommon.conversation.ConversationProperties;

public interface IConversationInfoKeeper extends IUserInfoChangeListener {

	ConversationId addConversation(ConversationProperties properties);

	void updateConversation(ConversationId id, ConversationProperties newProperties);

	boolean removeConversation(ConversationId id);

	Conversation getConversation(ConversationId conversationId);

	Collection<Conversation> getAvailableConversations(IConversationVisibilityDeterminer visibilityDeterminer);
}

