package simserver.history.userinfo;

import simcommon.conversation.Conversation;

public interface IConversationVisibilityDeterminer {

	boolean isVisible(Conversation conversation);
}
