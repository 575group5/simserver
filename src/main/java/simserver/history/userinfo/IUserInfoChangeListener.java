package simserver.history.userinfo;

import simcommon.entities.User;

public interface IUserInfoChangeListener {

	void onUserAdded(User user);

	void onUserRemoved(User user);
}
