package simserver.history.userinfo;

import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.utilities.ParamUtil;

public class UserBasedConversationVisibilityDeterminer implements IConversationVisibilityDeterminer {

	private final User mTargetUser;

	public UserBasedConversationVisibilityDeterminer(User targetUser) {
		ParamUtil.validateParamNotNull("must specify target user", targetUser);
		mTargetUser = targetUser;
	}
	
	@Override
	public boolean isVisible(Conversation conversation) {
		return conversation.isPublic() || conversation.getParticipants().contains(mTargetUser);
	}

}
