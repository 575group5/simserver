package simserver.server;

import simcommon.Constants;
import simcommon.entities.IServerInfo;
import simcommon.entities.StandardServerInfo;
import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.serialization.DelegatingMessageDeserializer;
import simcommon.serialization.DelegatingMessageSerializer;
import simcommon.serialization.IMessageDeserializer;
import simcommon.serialization.IMessageSerializer;
import simcommon.serialization.ISpecificMessageSerializerDeserializerFactory;
import simcommon.serialization.simple.factories.SimpleAuthenticateRequestMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleAuthenticateResponseMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleClientSendMessageMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleDirectoryRequestMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleDirectoryResponseMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleDisconnectMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleServerFailureMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleServerSendMessageMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleServerSuccessMessageSerializationFactory;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simserver.client.ClientAcceptor;
import simserver.client.SocketClientAcceptor;
import simserver.handlerchain.HandlerChain;
import simserver.handlerchain.HandlerChainBuilder;
import simserver.handlerchain.IMessageHandlerLogic;
import simserver.handlerchain.TypeMatchingMessageCheck;
import simserver.handlerchain.handlers.ConsolePrintSystemMessageLogger;
import simserver.handlerchain.handlers.ConversationAuthorizationChecker;
import simserver.handlerchain.handlers.ConversationHistoryRecorder;
import simserver.handlerchain.handlers.DirectoryRequestResponder;
import simserver.handlerchain.handlers.MessageDistributer;
import simserver.handlerchain.handlers.UnhandledMessageHandler;
import simserver.handlerchain.handlers.UserDisconnectedConnectionTerminatingHandler;
import simserver.handlerchain.handlers.UserDisconnectedInfoUpdatingHandler;
import simserver.handlerchain.handlers.authentication.NameOnlyAuthenticationEstablisher;
import simserver.handlerchain.handlers.identification.UserIdChecker;
import simserver.history.conversation.IConversationHistoryKeeper;
import simserver.history.conversation.NoHistoryHistoryKeeper;
import simserver.history.userinfo.IConversationInfoKeeper;
import simserver.history.userinfo.InMemoryConversationInfoKeeper;
import simserver.history.userinfo.InMemoryUserInfoKeeper;
import simserver.history.userinfo.UserInfoKeeper;

public class SimServerLauncher extends ServerLauncher {

	private IMessageSerializer mSerializer;
	private IMessageDeserializer mDeserializer;

	public SimServerLauncher() {
		setUpSerializerAndDeserializer();
	}

	@Override
	public IServerInfo createServerInfo() {
		return new StandardServerInfo(Constants.SIM_SERVER_PORT_NUMBER);
	}

	@Override
	public ClientAcceptor createClientAcceptor() {
		return new SocketClientAcceptor(createMessageSerializer(), createMessageDeserializer());
	}

	@Override
	public HandlerChain createHandlerChain(ISystemMessageSender systemMessageSender, int connectionId) {
		IMessageHandlerLogic logger = new ConsolePrintSystemMessageLogger();
		UserIdChecker userIdChecker = new UserIdChecker(systemMessageSender, mUserInfoKeeper);
		  return new HandlerChainBuilder()
		  .forCheckInOrder(new TypeMatchingMessageCheck(AuthenticationRequestSystemMessage.class))
		     .addHandlerInOrder(logger)
				.addHandlerInOrder(new NameOnlyAuthenticationEstablisher(systemMessageSender, userIdChecker,
						mUserInfoKeeper, mClientConnectionManager, connectionId))
		  .forCheckInOrder(new TypeMatchingMessageCheck(DirectoryRequestSystemMessage.class))
		     .addHandlerInOrder(logger)
		     .addHandlerInOrder(userIdChecker)
			 .addHandlerInOrder(new DirectoryRequestResponder(systemMessageSender, mConversationInfoKeeper))
		  .forCheckInOrder(new TypeMatchingMessageCheck(ClientSendMessageRequestSystemMessage.class))
		     .addHandlerInOrder(logger)
		     .addHandlerInOrder(userIdChecker)
			 .addHandlerInOrder(new ConversationAuthorizationChecker(systemMessageSender))
			 .addHandlerInOrder(new MessageDistributer(mClientConnectionManager))
			 .addHandlerInOrder(new ConversationHistoryRecorder(mConversationHistoryKeeper))
		  .forCheckInOrder(new TypeMatchingMessageCheck(ClientDisconnectRequestSystemMessage.class))
			 .addHandlerInOrder(logger)
			 .addHandlerInOrder(userIdChecker)
			 .addHandlerInOrder(new UserDisconnectedConnectionTerminatingHandler(connectionId, mClientConnectionManager))
			 .addHandlerInOrder(new UserDisconnectedInfoUpdatingHandler(mUserInfoKeeper))
		  .forDefault()
		     .addHandlerInOrder(logger)
			 .addHandlerInOrder(new UnhandledMessageHandler(systemMessageSender))
		  .build();
	}

	@Override
	public UserInfoKeeper createUserInfoKeeper() {
		return new InMemoryUserInfoKeeper();
	}

	@Override
	public IConversationInfoKeeper createConversationInfoKeeper() {
		return new InMemoryConversationInfoKeeper(mUserInfoKeeper);
	}

	@Override
	public IConversationHistoryKeeper createConversationHistoryKeeper() {
		return new NoHistoryHistoryKeeper();
	}

	@Override
	public IMessageSerializer createMessageSerializer() {
		return mSerializer;
	}

	@Override
	public IMessageDeserializer createMessageDeserializer() {
		return mDeserializer;
	}

	private void setUpSerializerAndDeserializer() {
		DelegatingMessageDeserializer.Builder deserializerBuilder = new DelegatingMessageDeserializer.Builder();
		DelegatingMessageSerializer.Builder serializerBuilder = new DelegatingMessageSerializer.Builder();
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleAuthenticateRequestMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleAuthenticateResponseMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleDirectoryRequestMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleDirectoryResponseMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleClientSendMessageMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleServerSendMessageMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleServerFailureMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleServerSuccessMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleDisconnectMessageSerializationFactory());
		mDeserializer = deserializerBuilder.build();
		mSerializer = serializerBuilder.build();
	}

	private void addSerializerAndDeserializerFromFactory(DelegatingMessageDeserializer.Builder deserializerBuilder,
			DelegatingMessageSerializer.Builder serializerBuilder,
			ISpecificMessageSerializerDeserializerFactory factory) {
		deserializerBuilder.add(factory.getSpecificDeserializer());
		serializerBuilder.add(factory.getSpecificSerializer());
	}
}
