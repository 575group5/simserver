package simserver.server;

import simcommon.entities.IServerInfo;
import simcommon.serialization.IMessageDeserializer;
import simcommon.serialization.IMessageSerializer;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.systemmessagecentral.SystemMessageCentral;
import simserver.client.ClientAcceptor;
import simserver.client.ClientAcceptor.AcceptClientException;
import simserver.client.ClientConnectionManager;
import simserver.client.ClientHandler;
import simserver.client.IClientAcceptedCallback;
import simserver.handlerchain.HandlerChain;
import simserver.history.conversation.IConversationHistoryKeeper;
import simserver.history.userinfo.IConversationInfoKeeper;
import simserver.history.userinfo.UserInfoKeeper;

public abstract class ServerLauncher implements IClientAcceptedCallback {

	protected UserInfoKeeper mUserInfoKeeper;
	protected IConversationInfoKeeper mConversationInfoKeeper;
	protected IConversationHistoryKeeper mConversationHistoryKeeper;

	protected ClientAcceptor mClientAcceptor;
	protected ClientConnectionManager mClientConnectionManager;
	private IServerInfo serverInfo;

	public void launchServer() throws AcceptClientException {
		mUserInfoKeeper = createUserInfoKeeper();
		mConversationInfoKeeper = createConversationInfoKeeper();
		mConversationHistoryKeeper = createConversationHistoryKeeper();
		serverInfo = createServerInfo();
		mClientConnectionManager = new ClientConnectionManager();
		mClientAcceptor = createClientAcceptor();
		mClientAcceptor.attachClientAcceptedCallback(this);
		mClientAcceptor.startAcceptingClients(serverInfo);
	}

	@Override
	public void onClientAccepted(SystemMessageCentral sysMsgCentral) {
		ClientHandler clientHandler = new ClientHandler(sysMsgCentral);
		Integer connectionId = mClientConnectionManager.generateConnectionId();
		HandlerChain handlerChain = createHandlerChain(sysMsgCentral, connectionId);
		clientHandler.attachHandlerChain(handlerChain);
		mClientConnectionManager.addClientHandler(connectionId, clientHandler);
	}

	protected abstract IServerInfo createServerInfo();

	protected abstract ClientAcceptor createClientAcceptor();

	protected abstract HandlerChain createHandlerChain(ISystemMessageSender systemMessageSender, int connectionId);

	protected abstract UserInfoKeeper createUserInfoKeeper();

	protected abstract IConversationInfoKeeper createConversationInfoKeeper();

	protected abstract IConversationHistoryKeeper createConversationHistoryKeeper();

	protected abstract IMessageSerializer createMessageSerializer();

	protected abstract IMessageDeserializer createMessageDeserializer();
}
