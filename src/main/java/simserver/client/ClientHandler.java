package simserver.client;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.ISystemMessageReceivedListener;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.systemmessagecentral.SystemMessageCentral;
import simcommon.utilities.ParamUtil;
import simserver.handlerchain.HandlerChain;

public class ClientHandler implements ISystemMessageReceivedListener {

	private SystemMessageCentral mSystemMessageCentral;
	private HandlerChain mHandlerChain;

	public ClientHandler(SystemMessageCentral systemMessageCentral) {
		ParamUtil.validateParamNotNull("must have SystemMessageCentral", systemMessageCentral);
		mSystemMessageCentral = systemMessageCentral;
	}

	public void attachHandlerChain(HandlerChain handlerChain) {
		ParamUtil.validateParamNotNull("handler chain must be non-null", handlerChain);
		mHandlerChain = handlerChain;
	}

	public ISystemMessageSender getMessageSender() {
		return mSystemMessageCentral;
	}

	public void start() {
		if (mHandlerChain == null) {
			throw new IllegalStateException("Handler chain must be attached before starting the client handler");
		}
		mSystemMessageCentral.attachSystemMessageReceivedListener(this);
		mSystemMessageCentral.startListeningForMessages();
	}

	public void stop() {
		mSystemMessageCentral.shutDown();
	}

	@Override
	public void onMessageReceived(SystemMessage message) {
		mHandlerChain.handleMessage(message);
	}
}
