package simserver.client;

import simcommon.entities.IServerInfo;
import simcommon.serialization.IMessageDeserializer;
import simcommon.serialization.IMessageSerializer;
import simcommon.systemmessagecentral.SystemMessageCentral;
import simcommon.utilities.ParamUtil;

public abstract class ClientAcceptor {

	// note: may be null if none has been attached
	private IClientAcceptedCallback mClientAcceptedCallback;
	protected IMessageSerializer mMessageSerializer;
	protected IMessageDeserializer mMessageDeserializer;

	public ClientAcceptor(IMessageSerializer messageSerializer, IMessageDeserializer messageDeserilizer) {
		ParamUtil.validateParamNotNull("must have serializer", messageSerializer);
		ParamUtil.validateParamNotNull("must have deserializer", messageDeserilizer);

		mMessageSerializer = messageSerializer;
		mMessageDeserializer = messageDeserilizer;
	}

	public void attachClientAcceptedCallback(IClientAcceptedCallback clientAcceptedCallback) {
		ParamUtil.validateParamNotNull("must have callback", clientAcceptedCallback);
		mClientAcceptedCallback = clientAcceptedCallback;
	}

	public abstract void startAcceptingClients(IServerInfo serverInfo) throws AcceptClientException;

	public abstract void stopAcceptingClients();

	protected void notifyClientAccepted(SystemMessageCentral systemMessageCentral) {
		if (mClientAcceptedCallback != null) {
			mClientAcceptedCallback.onClientAccepted(systemMessageCentral);
		}
	}

	public class AcceptClientException extends Exception {
		public AcceptClientException(String message, Exception cause) {
			super(message, cause);
		}
	}
}
