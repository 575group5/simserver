package simserver.client;

import simcommon.systemmessagecentral.SystemMessageCentral;

public interface IClientAcceptedCallback {

	void onClientAccepted(SystemMessageCentral sysMsgCentral);
}
