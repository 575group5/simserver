package simserver.client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import simcommon.entities.IServerInfo;
import simcommon.entities.StandardServerInfo;
import simcommon.serialization.IMessageDeserializer;
import simcommon.serialization.IMessageSerializer;
import simcommon.systemmessagecentral.SocketBasedSystemMessageCentral;
import simcommon.utilities.ParamUtil;

public class SocketClientAcceptor extends ClientAcceptor {

	public SocketClientAcceptor(IMessageSerializer messageSerializer, IMessageDeserializer messageDeserilizer) {
		super(messageSerializer, messageDeserilizer);
	}

	protected ServerSocket mServerSocket;
	private boolean checkForClients = true;

	@Override
	public void startAcceptingClients(IServerInfo serverInfo) throws AcceptClientException {
		ParamUtil.validateParamNotNull("must have server info", serverInfo);
		ParamUtil.validateConditionTrue("SocketClientAcceptor must be used with StandardServerInfo",
				serverInfo instanceof StandardServerInfo);

		int port = ((StandardServerInfo) serverInfo).getPort();

		try {
			mServerSocket = new ServerSocket(port);
		} catch (IOException e) {
			throw new AcceptClientException("could not create server socket on port:" + port, e);
		}
		System.out.println("AWAITING CONNECTION");
		acceptClients();
	}

	private void acceptClients() {
		while (checkForClients) {
			System.out.println("checking...");
			try {
				Socket socket = mServerSocket.accept();
				Thread runner = new Thread(new CheckForClientsRunnable(socket));
				runner.start();

			} catch (IOException e) {
			}
			System.out.println("CONNECTION ESTABLISHED");
		}
	}

	@Override
	public void stopAcceptingClients() {
		checkForClients = false;
		try {
			// closing socket from other thread should terminate the blocking
			// accept() call in CheckForClientsRunnable
			mServerSocket.close();
			System.out.println("closed socket");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class CheckForClientsRunnable implements Runnable {

		private Socket socket;

		public CheckForClientsRunnable(Socket socket) {
			this.socket = socket;

		}

		@Override
		public void run() {
			try {
				notifyClientAccepted(
						new SocketBasedSystemMessageCentral(mMessageSerializer, mMessageDeserializer, socket));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
