package simserver.handlerchain;

import simcommon.messages.systemMessages.SystemMessage;

public interface IMessageHandlerLogic {
	boolean handle(SystemMessage message);
}
