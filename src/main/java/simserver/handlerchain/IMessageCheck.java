package simserver.handlerchain;

import simcommon.messages.systemMessages.SystemMessage;

public interface IMessageCheck {

	boolean accepts(SystemMessage message);
}
