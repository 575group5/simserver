package simserver.handlerchain.handlers;

import simcommon.entities.User;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.systemMessages.source.UserMessageSource;
import simcommon.utilities.ParamUtil;
import simserver.handlerchain.IMessageHandlerLogic;
import simserver.history.userinfo.UserInfoKeeper;

public class UserDisconnectedInfoUpdatingHandler implements IMessageHandlerLogic {

	private UserInfoKeeper mUserInfoKeeper;

	public UserDisconnectedInfoUpdatingHandler(UserInfoKeeper userInfoKeeper) {
		ParamUtil.validateParamNotNull("must have user info keeper", userInfoKeeper);
		mUserInfoKeeper = userInfoKeeper;
	}

	@Override
	public boolean handle(SystemMessage message) {
		ParamUtil.validateParamNotNull("message can't be null", message);
		ParamUtil.validateConditionTrue("we only handle disconnects from users",
				message.getMessageSource() instanceof UserMessageSource);
		User disconnectingUser = ((UserMessageSource) message.getMessageSource()).getUser();
		mUserInfoKeeper.removeUser(disconnectingUser.getUserId());
		return true;
	}
}
