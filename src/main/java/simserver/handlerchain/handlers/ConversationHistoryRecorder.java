package simserver.handlerchain.handlers;

import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.utilities.ParamUtil;
import simserver.handlerchain.IMessageHandlerLogic;
import simserver.history.conversation.IConversationHistoryKeeper;

public class ConversationHistoryRecorder implements IMessageHandlerLogic {

	private IConversationHistoryKeeper mConversationHistoryKeeper;

	public ConversationHistoryRecorder(IConversationHistoryKeeper conversationHistoryKeeper) {
		ParamUtil.validateParamNotNull("must have history keeper", conversationHistoryKeeper);
		mConversationHistoryKeeper = conversationHistoryKeeper;
	}

	@Override
	public boolean handle(SystemMessage message) {
		ParamUtil.validateConditionTrue("only accepts ClientSendMessageRequestSystemMessage",
				message instanceof ClientSendMessageRequestSystemMessage);
		ClientSendMessageRequestSystemMessage clientSendMessageRequest = (ClientSendMessageRequestSystemMessage) message;
		mConversationHistoryKeeper.recordMessageSent(clientSendMessageRequest.getConversation(),
				clientSendMessageRequest.getMessageOrigin(), clientSendMessageRequest.getUserMessage());
		return true;
	}

}
