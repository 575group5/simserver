package simserver.handlerchain.handlers;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.utilities.ParamUtil;
import simserver.client.ClientConnectionManager;
import simserver.handlerchain.IMessageHandlerLogic;

public class UserDisconnectedConnectionTerminatingHandler implements IMessageHandlerLogic {

	private int mConectionId;
	private ClientConnectionManager mClientConnectionManager;
	
	public UserDisconnectedConnectionTerminatingHandler(int connectionId,
			ClientConnectionManager clientConnectionManager) {
		ParamUtil.validateParamNotNull("must have connection manager", clientConnectionManager);
		mConectionId = connectionId;
		mClientConnectionManager = clientConnectionManager;
	}

	@Override
	public boolean handle(SystemMessage message) {
		mClientConnectionManager.removeClientHandler(mConectionId);
		return true;
	}
}
