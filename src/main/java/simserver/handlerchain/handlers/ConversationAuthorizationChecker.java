package simserver.handlerchain.handlers;

import simcommon.FailReason;
import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.userMessages.origin.UserOrigin;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.utilities.ParamUtil;
import simserver.handlerchain.IMessageHandlerLogic;

public class ConversationAuthorizationChecker implements IMessageHandlerLogic {

	protected ISystemMessageSender mSystemMessageSender;

	public ConversationAuthorizationChecker(ISystemMessageSender systemMessageSender) {
		ParamUtil.validateParamNotNull("must have system message sender", systemMessageSender);
		mSystemMessageSender = systemMessageSender;
	}
	
	@Override
	public boolean handle(SystemMessage message) {
		ParamUtil.validateConditionTrue("we only accept ClientSendMessageRequestSystemMessage", message instanceof ClientSendMessageRequestSystemMessage);
		ClientSendMessageRequestSystemMessage clientSendMessageRequest = (ClientSendMessageRequestSystemMessage) message;
		User source = ((UserOrigin) clientSendMessageRequest.getMessageOrigin()).getOriginUser();
		Conversation target = clientSendMessageRequest.getConversation();
		boolean userAuthorized = target.getParticipants().contains(source);
		if (!userAuthorized) {
			mSystemMessageSender.sendMessage(
					new GeneralServerFailureResponseSystemMessage(FailReason.USER_NOT_AUTHORIZED_FOR_ACTION), null);
		}
		return userAuthorized;
	}
}
