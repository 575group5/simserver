package simserver.handlerchain.handlers.authentication;

import simcommon.FailReason;
import simcommon.authentication.IUserAuthentication;
import simcommon.entities.User;
import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.utilities.ParamUtil;
import simserver.handlerchain.IMessageHandlerLogic;

public abstract class AuthenticationEstablisher implements IMessageHandlerLogic {

	protected AuthenticationEstablisherState mState = new UnauthenticatedState();
	protected ISystemMessageSender mSystemMessageSender;
	
	public AuthenticationEstablisher(ISystemMessageSender systemMessageSender) {
		ParamUtil.validateParamNotNull("must have system message sender", systemMessageSender);
		mSystemMessageSender = systemMessageSender;
	}

	@Override
	public boolean handle(SystemMessage message) {
		ParamUtil.validateConditionTrue("we only accept AuthenticationRequestSystemMessages",
				message instanceof AuthenticationRequestSystemMessage);
		return mState.handle((AuthenticationRequestSystemMessage) message);
	}
	
	/**
	 * @return User if authenticated, or null if authentication failed
	 */
	public abstract User authenticate(IUserAuthentication authInfo);

	private interface AuthenticationEstablisherState {

		boolean handle(AuthenticationRequestSystemMessage message);
	}

	public class AlreadyAuthenticatedState implements AuthenticationEstablisherState {

		@Override
		public boolean handle(AuthenticationRequestSystemMessage message) {
			mSystemMessageSender.sendMessage(
					new GeneralServerFailureResponseSystemMessage(FailReason.UNEXPECTD_MESSAGE), null);
			return false;
		}
	}

	public class UnauthenticatedState implements AuthenticationEstablisherState {

		@Override
		public boolean handle(AuthenticationRequestSystemMessage message) {
			User user = authenticate(message.getAuthentication());
			if (user == null) {
				// authentication failed
				mSystemMessageSender.sendMessage(new AuthenticationResponseSystemMessage(), null);
				return false;
			} else {
				AuthenticationResponseSystemMessage response = new AuthenticationResponseSystemMessage(user);
				mSystemMessageSender.sendMessage(response, null);
				mState = new AlreadyAuthenticatedState();
				return true;
			}
		}
	}
}
