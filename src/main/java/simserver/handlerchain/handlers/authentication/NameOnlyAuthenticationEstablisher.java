package simserver.handlerchain.handlers.authentication;

import java.util.Collection;

import simcommon.authentication.IUserAuthentication;
import simcommon.authentication.UsernameAuthentication;
import simcommon.entities.User;
import simcommon.entities.UserInfo;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.utilities.ParamUtil;
import simserver.client.ClientConnectionManager;
import simserver.handlerchain.handlers.identification.UserIdChecker;
import simserver.history.userinfo.UserInfoKeeper;

public class NameOnlyAuthenticationEstablisher extends AuthenticationEstablisher {

	private UserIdChecker mUserIdChecker;
	private int mConnectionId;
	private ClientConnectionManager mClientConnectionManager;
	private UserInfoKeeper mUserInfoKeeper;

	public NameOnlyAuthenticationEstablisher(ISystemMessageSender systemMessageSender, UserIdChecker userIdChecker,
			UserInfoKeeper userInfoKeeper, ClientConnectionManager clientConnectionManager, int connectionId) {
		super(systemMessageSender);
		ParamUtil.validateParamNotNull("must have user id checker", userIdChecker);
		ParamUtil.validateParamNotNull("must have info keeper", userInfoKeeper);
		ParamUtil.validateParamNotNull("must have connection manager", clientConnectionManager);
		mUserIdChecker = userIdChecker;
		mUserInfoKeeper = userInfoKeeper;
		mClientConnectionManager = clientConnectionManager;
		mConnectionId = connectionId;
	}

	@Override
	public User authenticate(IUserAuthentication authInfo) {
		ParamUtil.validateParamNotNull("must have auth info", authInfo);
		ParamUtil.validateConditionTrue("we only handle UsernameAuthentication",
				authInfo instanceof UsernameAuthentication);

		String prospectiveUserName = ((UsernameAuthentication) authInfo).getUsername();
		if (prospectiveUserName.isEmpty()) {
			return null;
		}

		Collection<User> allUsers = mUserInfoKeeper.getAllUsers();
		for (User user : allUsers) {
			if (user.getUserInfo().getName().equals(prospectiveUserName)) {
				return null;
			}
		}

		User user = mUserInfoKeeper.addUser(new UserInfo(prospectiveUserName));
		mUserIdChecker.initializeWithUserId(user);
		mClientConnectionManager.associateUserIdWithConnection(mConnectionId, user.getUserId());
		return user;
	}
}
