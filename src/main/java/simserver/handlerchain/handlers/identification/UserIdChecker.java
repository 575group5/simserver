package simserver.handlerchain.handlers.identification;

import simcommon.FailReason;
import simcommon.entities.User;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.systemMessages.source.UserMessageSource;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.utilities.ParamUtil;
import simserver.handlerchain.IMessageHandlerLogic;
import simserver.history.userinfo.IUserInfoChangeListener;
import simserver.history.userinfo.UserInfoKeeper;

public class UserIdChecker implements IMessageHandlerLogic {

	protected IUserIdCheckerState mState = new UninitializedCheckerState();
	protected ISystemMessageSender mSystemMessageSender;

	public UserIdChecker(ISystemMessageSender systemMessageSender, UserInfoKeeper userInfoKeeper) {
		ParamUtil.validateParamNotNull("must have message sender", systemMessageSender);
		ParamUtil.validateParamNotNull("must have user info keeper", userInfoKeeper);
		mSystemMessageSender = systemMessageSender;
		userInfoKeeper.addListener(new IUserInfoChangeListener() {

			@Override
			public void onUserRemoved(User user) {
				mState.handleUserRemoved(user);
			}

			@Override
			public void onUserAdded(User user) {
			}
		});
	}

	@Override
	public boolean handle(SystemMessage message) {
		ParamUtil.validateParamNotNull("message cannot be null", message);
		return mState.handle(message);
	}

	public void initializeWithUserId(User user) {
		mState.initializeWithUser(user);
	}

	private interface IUserIdCheckerState {

		boolean handle(SystemMessage message);

		void initializeWithUser(User user);

		void handleUserRemoved(User removed);
	}

	private class InitializedCheckerState implements IUserIdCheckerState {

		private User mUser;

		public InitializedCheckerState(User user) {
			ParamUtil.validateParamNotNull("must have user", user);
			mUser = user;
		}

		@Override
		public boolean handle(SystemMessage message) {
			// The message here should be a message from specific user. if it's
			// not, something was set up wrong when this checker was attached
			ParamUtil.validateConditionTrue("UserIdChecker only handles messages from specific user",
					message.getMessageSource() instanceof UserMessageSource);
			UserMessageSource specificUserSourceInfo = (UserMessageSource) message.getMessageSource();
			if (specificUserSourceInfo.getUser().getUserId().equals(mUser.getUserId())) {
				// successful match, pass it along
				return true;
			} else {
				// send an error message and abort the sequence
				mSystemMessageSender
						.sendMessage(new GeneralServerFailureResponseSystemMessage(FailReason.MISMATCH_USER), null);
				return false;
			}
		}

		@Override
		public void initializeWithUser(User user) {
			throw new IllegalStateException("User Id Checker already initialized");
		}

		@Override
		public void handleUserRemoved(User removed) {
			if (mUser.equals(removed)) {
				mState = new UninitializedCheckerState();
			}
		}

	}

	private class UninitializedCheckerState implements IUserIdCheckerState {

		@Override
		public boolean handle(SystemMessage message) {
			// we're receiving a "user specific" message out of sequence, before
			// this user has been authenticated
			mSystemMessageSender.sendMessage(
					new GeneralServerFailureResponseSystemMessage(FailReason.USER_NOT_AUTHENTICATED), null);
			return false;
		}

		@Override
		public void initializeWithUser(User user) {
			mState = new InitializedCheckerState(user);
		}

		@Override
		public void handleUserRemoved(User removed) {
			// not relevant
		}

	}
}
