package simserver.handlerchain.handlers;

import java.io.PrintStream;

public class ConsolePrintSystemMessageLogger extends SystemMessageLogger {

	@Override
	public PrintStream getOutputStream() {
		return System.out;
	}
}
