package simserver.handlerchain.handlers;

import java.util.Collection;

import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.DirectoryResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.utilities.ParamUtil;
import simserver.handlerchain.IMessageHandlerLogic;
import simserver.history.userinfo.IConversationInfoKeeper;
import simserver.history.userinfo.UserBasedConversationVisibilityDeterminer;

public class DirectoryRequestResponder implements IMessageHandlerLogic {

	private IConversationInfoKeeper mConversationInfoKeeper;
	private ISystemMessageSender mSystemMessageSender;

	public DirectoryRequestResponder(ISystemMessageSender systemMessageSender,
			IConversationInfoKeeper conversationInfoKeeper) {
		ParamUtil.validateParamNotNull("must have system message sender", systemMessageSender);
		ParamUtil.validateParamNotNull("must have conversation info keeper", conversationInfoKeeper);
		mSystemMessageSender = systemMessageSender;
		mConversationInfoKeeper = conversationInfoKeeper;
	}

	@Override
	public boolean handle(SystemMessage message) {
		ParamUtil.validateParamNotNull("message cannot be null", message);
		ParamUtil.validateConditionTrue("DirectoryRequestResponder only handles DirectoryRequestSystemMessages",
				message instanceof DirectoryRequestSystemMessage);
		User source = ((DirectoryRequestSystemMessage) message).getSource();
		Collection<Conversation> conversations = mConversationInfoKeeper
				.getAvailableConversations(new UserBasedConversationVisibilityDeterminer(source));
		mSystemMessageSender.sendMessage(new DirectoryResponseSystemMessage(conversations), null);
		return true;
	}
}
