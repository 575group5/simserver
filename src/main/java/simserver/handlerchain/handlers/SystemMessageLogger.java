package simserver.handlerchain.handlers;

import java.io.PrintStream;

import simcommon.messages.systemMessages.SystemMessage;
import simserver.handlerchain.IMessageHandlerLogic;

public abstract class SystemMessageLogger implements IMessageHandlerLogic {

	public abstract PrintStream getOutputStream();
	
	@Override
	public boolean handle(SystemMessage message) {
		getOutputStream().println(
				"server received message of type: " + message.getClass());
		return true;
	}
}