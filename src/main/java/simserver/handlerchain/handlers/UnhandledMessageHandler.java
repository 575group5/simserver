package simserver.handlerchain.handlers;

import simcommon.FailReason;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.utilities.ParamUtil;
import simserver.handlerchain.IMessageHandlerLogic;

public class UnhandledMessageHandler implements IMessageHandlerLogic {

	protected ISystemMessageSender mSystemMessageSender;

	public UnhandledMessageHandler(ISystemMessageSender systemMessageSender) {
		ParamUtil.validateParamNotNull("must have system message sender", systemMessageSender);
		mSystemMessageSender = systemMessageSender;
	}
	
	@Override
	public boolean handle(SystemMessage message) {
		mSystemMessageSender.sendMessage(new GeneralServerFailureResponseSystemMessage(FailReason.UNRECOGNIZED_REQUEST),
				null);
		return true;
	}
}
