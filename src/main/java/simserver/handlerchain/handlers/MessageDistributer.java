package simserver.handlerchain.handlers;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.collections4.CollectionUtils;

import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.GeneralServerSuccessResponseSystemMessage;
import simcommon.messages.systemMessages.ServerSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.UserOrigin;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.utilities.ParamUtil;
import simserver.client.ClientConnectionManager;
import simserver.handlerchain.IMessageHandlerLogic;

public class MessageDistributer implements IMessageHandlerLogic {

	private ClientConnectionManager mClientConnectionManager;

	public MessageDistributer(ClientConnectionManager clientConnectionManager) {
		ParamUtil.validateParamNotNull("need connection manager", clientConnectionManager);
		mClientConnectionManager = clientConnectionManager;
	}
	
	@Override
	public boolean handle(SystemMessage message) {
		ParamUtil.validateConditionTrue("we only handle ClientSendMessageRequestSystemMessage",
				message instanceof ClientSendMessageRequestSystemMessage);
		ClientSendMessageRequestSystemMessage clientSendMessageRequest = (ClientSendMessageRequestSystemMessage) message;
		User sender = getSender(clientSendMessageRequest);
		Collection<User> recipients = getRecipients(clientSendMessageRequest);
		for (User recipient : recipients) {
			sendMessageToRecipient(sender, recipient, clientSendMessageRequest.getUserMessage(),
					clientSendMessageRequest.getConversation());
		}
		sendSystemMessage(sender, new GeneralServerSuccessResponseSystemMessage());
		return true;
	}

	private User getSender(ClientSendMessageRequestSystemMessage clientSendRequest) {
		return clientSendRequest.getMessageOrigin().getOriginUser();
	}

	private Collection<User> getRecipients(ClientSendMessageRequestSystemMessage clientSendRequest) {
		Collection<User> allConversationParticipants = clientSendRequest.getConversation().getParticipants();
		return CollectionUtils.subtract(allConversationParticipants,
				Collections.singleton(getSender(clientSendRequest)));
	}

	private void sendMessageToRecipient(User source, User recipient, IUserMessage message, Conversation conversation) {
		SystemMessage sendMessage = new ServerSendMessageRequestSystemMessage(new UserOrigin(source), conversation,
				message);
		sendSystemMessage(recipient, sendMessage);
	}

	private void sendSystemMessage(User recipient, SystemMessage message) {
		ISystemMessageSender clientMessageSender = mClientConnectionManager
				.getClientHandlerForUser(recipient.getUserId()).getMessageSender();
		clientMessageSender.sendMessage(message, null);
	}
}
