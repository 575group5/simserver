package simserver.handlerchain;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.tuple.Pair;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.utilities.ParamUtil;

public class HandlerChain {

	private final List<Pair<IMessageCheck, MessageHandler>> mChecksInOrder;

	protected HandlerChain(HandlerChainBuilder builder) {
		ParamUtil.validateParamNotNull("null builder not allowed", builder);
		// copying it into an array list means changes to the builder's list
		// won't affect our list
		List<Pair<IMessageCheck, MessageHandler>> checksInOrder = new ArrayList<>(builder.getContents());
		// storing an unmodifiable list enforces that we can't change it after
		// this point
		mChecksInOrder = ListUtils.unmodifiableList(checksInOrder);
	}
	
	public void handleMessage(SystemMessage systemMessage){
		ParamUtil.validateParamNotNull("cannot handle null message", systemMessage);
		for (Pair<IMessageCheck, MessageHandler> entry : mChecksInOrder) {
			if (entry.getKey().accepts(systemMessage)) {
				entry.getValue().handle(systemMessage);
				break;
			}
		}
	}
}
