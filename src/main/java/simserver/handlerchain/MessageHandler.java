package simserver.handlerchain;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.utilities.ParamUtil;

public class MessageHandler {

	private IMessageHandlerLogic mHandlerLogic;
	private MessageHandler mSubsequentHandler;

	public MessageHandler(IMessageHandlerLogic logic) {
		ParamUtil.validateParamNotNull("logic is required", logic);
		mHandlerLogic = logic;
	}

	public void handle(SystemMessage message) {
		boolean shouldNotify = mHandlerLogic.handle(message);
		if (shouldNotify) {
			notifySubsequent(message);
		}
	}

	public void attachSubsequent(MessageHandler handler) {
		ParamUtil.validateParamNotNull("subsequent handler cannot be null", handler);
		mSubsequentHandler = handler;
	}

	private void notifySubsequent(SystemMessage message) {
		if (mSubsequentHandler != null) {
			mSubsequentHandler.handle(message);
		}
	}
}
