package simserver.handlerchain;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.utilities.ParamUtil;

public class HandlerChainBuilder {

	private boolean mBuilt;
	private List<Pair<IMessageCheck, MessageHandler>> mCheckAndHandlerChainsConstructedSoFar = new ArrayList<>();
	private CheckAndHandlers mCheckAndHandlersUnderConstruction;

	public HandlerChainBuilder forCheckInOrder(IMessageCheck messageCheck) {
		ParamUtil.validateParamNotNull("cannot add null message check", messageCheck);
		assertNotAlreadyBuilt();
		assertLastCheckHasHandler();
		finishLastCheckAndHandlersIfAny();
		mCheckAndHandlersUnderConstruction = new CheckAndHandlers(messageCheck);
		return this;
	}

	public HandlerChainBuilder addHandlerInOrder(IMessageHandlerLogic handlerLogic) {
		ParamUtil.validateParamNotNull("cannot add null message handler", handlerLogic);
		assertNotAlreadyBuilt();
		assertHasCheckUnderConstruction();
		mCheckAndHandlersUnderConstruction.addHandler(new MessageHandler(handlerLogic));
		return this;
	}

	public HandlerChainBuilder forDefault() {
		return forCheckInOrder(new AcceptEverythingCheck());
	}

	public HandlerChain build() {
		assertNotAlreadyBuilt();
		assertLastCheckHasHandler();
		finishLastCheckAndHandlersIfAny();
		HandlerChain result = new HandlerChain(this);
		mBuilt = true;
		return result;
	}

	/**
	 * For use by HandlerChain constructor ONLY.
	 */
	/* package */ List<Pair<IMessageCheck, MessageHandler>> getContents() {
		assertNotAlreadyBuilt();
		return mCheckAndHandlerChainsConstructedSoFar;
	}

	private void finishLastCheckAndHandlersIfAny() {
		if (mCheckAndHandlersUnderConstruction != null) {
			mCheckAndHandlerChainsConstructedSoFar.add(mCheckAndHandlersUnderConstruction.getAsPair());
		}
		mCheckAndHandlersUnderConstruction = null;
	}

	private void assertNotAlreadyBuilt() {
		if (mBuilt) {
			throw new IllegalStateException("this builder has already been built");
		}
	}

	private void assertLastCheckHasHandler() {
		if (mCheckAndHandlersUnderConstruction != null && !mCheckAndHandlersUnderConstruction.hasHandler()) {
			throw new IllegalStateException(
					"cannot add another check until specifying at least one handler for previous check");
		}
	}

	private void assertHasCheckUnderConstruction() {
		if (mCheckAndHandlersUnderConstruction == null) {
			throw new IllegalStateException("must specify a check before adding handlers");
		}
	}

	private class AcceptEverythingCheck implements IMessageCheck {

		@Override
		public boolean accepts(SystemMessage message) {
			return true;
		}

	}

	private class CheckAndHandlers {
		private IMessageCheck mCheck;
		private MessageHandler mFirstHandler;
		private MessageHandler mLastHandler;

		public CheckAndHandlers(IMessageCheck messageCheck) {
			mCheck = messageCheck;
		}

		public void addHandler(MessageHandler messageHandler) {
			if (mFirstHandler == null) {
				mFirstHandler = messageHandler;
				mLastHandler = messageHandler;
			} else {
				mLastHandler.attachSubsequent(messageHandler);
				mLastHandler = messageHandler;
			}
		}

		public boolean hasHandler() {
			return mFirstHandler != null;
		}

		public Pair<IMessageCheck, MessageHandler> getAsPair() {
			return Pair.of(mCheck, mFirstHandler);
		}
	}
}
