package simserver.handlerchain;

import simcommon.messages.systemMessages.SystemMessage;
import simcommon.utilities.ParamUtil;

public class TypeMatchingMessageCheck implements IMessageCheck {

	protected Class<? extends SystemMessage> mAcceptedMessageClass;
	
	public TypeMatchingMessageCheck(Class<? extends SystemMessage> clazz) {
		ParamUtil.validateParamNotNull("must have class", clazz);
		mAcceptedMessageClass = clazz;
	}

	@Override
	public boolean accepts(SystemMessage message) {
		return mAcceptedMessageClass.isInstance(message);
	}
}
