package simserver.handlerchain;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;

import simcommon.messages.systemMessages.SystemMessage;

public class HandlerChainTest {

	@Test
	public void testTypicalConfiguration_handlesAppropriateMessages_firstCheckAccepts() {
		IMessageCheck check1 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerA = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic handlerB = mock(IMessageHandlerLogic.class);
		IMessageCheck check2 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerC = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic defaultHandler = mock(IMessageHandlerLogic.class);

		HandlerChain handlerChain = new HandlerChainBuilder().forCheckInOrder(check1).addHandlerInOrder(handlerA)
				.addHandlerInOrder(handlerB).forCheckInOrder(check2).addHandlerInOrder(handlerC).forDefault()
				.addHandlerInOrder(defaultHandler).build();

		when(handlerA.handle(any(SystemMessage.class))).thenReturn(true);

		SystemMessage message = mock(SystemMessage.class);
		when(check1.accepts(message)).thenReturn(true);
		when(check2.accepts(message)).thenReturn(false);

		handlerChain.handleMessage(message);

		verify(handlerA, times(1)).handle(message);
		verify(handlerB, times(1)).handle(message);
		verify(handlerC, never()).handle(message);
		verify(defaultHandler, never()).handle(message);
	}

	@Test
	public void testTypicalConfiguration_handlesAppropriateMessages_secondCheckAccepts() {
		IMessageCheck check1 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerA = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic handlerB = mock(IMessageHandlerLogic.class);
		IMessageCheck check2 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerC = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic defaultHandler = mock(IMessageHandlerLogic.class);

		HandlerChain handlerChain = new HandlerChainBuilder().forCheckInOrder(check1).addHandlerInOrder(handlerA)
				.addHandlerInOrder(handlerB).forCheckInOrder(check2).addHandlerInOrder(handlerC).forDefault()
				.addHandlerInOrder(defaultHandler).build();

		when(handlerA.handle(any(SystemMessage.class))).thenReturn(true);

		SystemMessage message = mock(SystemMessage.class);
		when(check1.accepts(message)).thenReturn(false);
		when(check2.accepts(message)).thenReturn(true);

		handlerChain.handleMessage(message);

		verify(handlerA, never()).handle(message);
		verify(handlerB, never()).handle(message);
		verify(handlerC, times(1)).handle(message);
		verify(defaultHandler, never()).handle(message);
	}

	@Test
	public void testTypicalConfiguration_handlesAppropriateMessages_fallsThroughToDefault() {
		IMessageCheck check1 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerA = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic handlerB = mock(IMessageHandlerLogic.class);
		IMessageCheck check2 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerC = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic defaultHandler = mock(IMessageHandlerLogic.class);

		HandlerChain handlerChain = new HandlerChainBuilder().forCheckInOrder(check1).addHandlerInOrder(handlerA)
				.addHandlerInOrder(handlerB).forCheckInOrder(check2).addHandlerInOrder(handlerC).forDefault()
				.addHandlerInOrder(defaultHandler).build();

		when(handlerA.handle(any(SystemMessage.class))).thenReturn(true);

		SystemMessage message = mock(SystemMessage.class);
		when(check1.accepts(message)).thenReturn(false);
		when(check2.accepts(message)).thenReturn(false);

		handlerChain.handleMessage(message);

		verify(handlerA, never()).handle(message);
		verify(handlerB, never()).handle(message);
		verify(handlerC, never()).handle(message);
		verify(defaultHandler, times(1)).handle(message);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testTypicalConfiguration_errorOnNullMessage() {
		IMessageCheck check1 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerA = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic handlerB = mock(IMessageHandlerLogic.class);
		IMessageCheck check2 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerC = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic defaultHandler = mock(IMessageHandlerLogic.class);

		HandlerChain handlerChain = new HandlerChainBuilder().forCheckInOrder(check1).addHandlerInOrder(handlerA)
				.addHandlerInOrder(handlerB).forCheckInOrder(check2).addHandlerInOrder(handlerC).forDefault()
				.addHandlerInOrder(defaultHandler).build();

		handlerChain.handleMessage(null);
	}

	@Test
	public void testConfigurationWithNoDefault_ignoresUnhandledMessages() {
		IMessageCheck check1 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerA = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic handlerB = mock(IMessageHandlerLogic.class);
		IMessageCheck check2 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerC = mock(IMessageHandlerLogic.class);

		HandlerChain handlerChain = new HandlerChainBuilder().forCheckInOrder(check1).addHandlerInOrder(handlerA)
				.addHandlerInOrder(handlerB).forCheckInOrder(check2).addHandlerInOrder(handlerC).build();

		when(handlerA.handle(any(SystemMessage.class))).thenReturn(true);

		SystemMessage message = mock(SystemMessage.class);
		when(check1.accepts(message)).thenReturn(false);
		when(check2.accepts(message)).thenReturn(false);

		handlerChain.handleMessage(message);

		verify(handlerA, never()).handle(message);
		verify(handlerB, never()).handle(message);
		verify(handlerC, never()).handle(message);
	}

	@Test
	public void testConfigurationWithOnlyDefault_acceptsAllMessages() {
		IMessageHandlerLogic default1 = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic default2 = mock(IMessageHandlerLogic.class);

		HandlerChain handlerChain = new HandlerChainBuilder().forDefault().addHandlerInOrder(default1)
				.addHandlerInOrder(default2).build();

		when(default1.handle(any(SystemMessage.class))).thenReturn(true);

		SystemMessage message = mock(SystemMessage.class);
		handlerChain.handleMessage(message);

		verify(default1, times(1)).handle(message);
		verify(default2, times(1)).handle(message);
	}
	
	@Test
	public void testConfigurationWithNoChecks_doesNothing() {
		HandlerChain handlerChain = new HandlerChainBuilder().build();

		SystemMessage message = mock(SystemMessage.class);
		handlerChain.handleMessage(message);
	}
}
