package simserver.handlerchain.handlers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import simcommon.messages.userMessages.origin.UserOrigin;
import simserver.history.conversation.IConversationHistoryKeeper;

public class ConversationHistoryRecorderTest {

	private IConversationHistoryKeeper mHistoryKeeper = mock(IConversationHistoryKeeper.class);
	private ConversationHistoryRecorder mSubject = new ConversationHistoryRecorder(mHistoryKeeper);

	@Test(expected = IllegalArgumentException.class)
	public void testConstructor_nullParam() {
		new ConversationHistoryRecorder(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHandle_wrongMessageType() {
		mSubject.handle(new ClientDisconnectRequestSystemMessage(mock(User.class)));
	}

	@Test
	public void testHandle_recordsHistory() {
		User source = mock(User.class);
		Conversation conversation = mock(Conversation.class);
		IUserMessage message = mock(IUserMessage.class);
		ClientSendMessageRequestSystemMessage clientSendMessageRequest = new ClientSendMessageRequestSystemMessage(
				source, conversation, message);
		boolean result = mSubject.handle(clientSendMessageRequest);
		assertTrue(result);
		ArgumentCaptor<IUserMessageOrigin> messageOriginCaptor = ArgumentCaptor.forClass(IUserMessageOrigin.class);
		verify(mHistoryKeeper, times(1)).recordMessageSent(eq(conversation), messageOriginCaptor.capture(),
				eq(message));
		assertEquals(source, ((UserOrigin) messageOriginCaptor.getValue()).getOriginUser());
	}
}
