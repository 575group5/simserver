package simserver.handlerchain.handlers.authentication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import simcommon.FailReason;
import simcommon.authentication.IUserAuthentication;
import simcommon.entities.User;
import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.IStatusCallback;
import simcommon.systemmessagecentral.ISystemMessageSender;

public class AuthenticationEstablisherTest {

	private ISystemMessageSender mSender = mock(ISystemMessageSender.class);

	private IUserAuthentication mParamVal;
	private User mReturnVal;

	private AuthenticationEstablisher mSubject = new AuthenticationEstablisher(mSender) {

		@Override
		public User authenticate(IUserAuthentication authInfo) {
			mParamVal = authInfo;
			return mReturnVal;
		}
	};

	@Test(expected = IllegalArgumentException.class)
	public void testHandle_nullMessage() {
		mSubject.handle(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHandle_wrongMessage() {
		mSubject.handle(new ClientDisconnectRequestSystemMessage(mock(User.class)));
	}

	@Test
	public void testHandle_unauthenticated_becomesAuthenticated() {
		IUserAuthentication userAuthentication = mock(IUserAuthentication.class);
		AuthenticationRequestSystemMessage authenticationRequestSystemMessage = mock(
				AuthenticationRequestSystemMessage.class);
		when(authenticationRequestSystemMessage.getAuthentication()).thenReturn(userAuthentication);

		mReturnVal = mock(User.class);
		mSubject.handle(authenticationRequestSystemMessage);
		assertEquals(userAuthentication, mParamVal);
		ArgumentCaptor<SystemMessage> captor = ArgumentCaptor.forClass(SystemMessage.class);
		verify(mSender, times(1)).sendMessage(captor.capture(), any(IStatusCallback.class));
		assertTrue(captor.getValue() instanceof AuthenticationResponseSystemMessage);
		assertEquals(true, ((AuthenticationResponseSystemMessage) captor.getValue()).authenticationSuccessful());
		assertEquals(mReturnVal, ((AuthenticationResponseSystemMessage) captor.getValue()).getUser());
	}

	@Test
	public void testHandle_unauthenticated_authenticationFails() {
		IUserAuthentication userAuthentication = mock(IUserAuthentication.class);
		AuthenticationRequestSystemMessage authenticationRequestSystemMessage = mock(
				AuthenticationRequestSystemMessage.class);
		when(authenticationRequestSystemMessage.getAuthentication()).thenReturn(userAuthentication);

		mReturnVal = null;
		mSubject.handle(authenticationRequestSystemMessage);
		assertEquals(userAuthentication, mParamVal);
		ArgumentCaptor<SystemMessage> captor = ArgumentCaptor.forClass(SystemMessage.class);
		verify(mSender, times(1)).sendMessage(captor.capture(), any(IStatusCallback.class));
		assertTrue(captor.getValue() instanceof AuthenticationResponseSystemMessage);
		assertEquals(false, ((AuthenticationResponseSystemMessage) captor.getValue()).authenticationSuccessful());
	}

	@Test
	public void testHandle_alreadyAuthenticated() {
		IUserAuthentication userAuthentication = mock(IUserAuthentication.class);
		AuthenticationRequestSystemMessage authenticationRequestSystemMessage = mock(
				AuthenticationRequestSystemMessage.class);
		when(authenticationRequestSystemMessage.getAuthentication()).thenReturn(userAuthentication);

		mReturnVal = mock(User.class);
		mSubject.handle(authenticationRequestSystemMessage);

		mSubject.handle(authenticationRequestSystemMessage);

		ArgumentCaptor<SystemMessage> captor = ArgumentCaptor.forClass(SystemMessage.class);
		verify(mSender, times(2)).sendMessage(captor.capture(), any(IStatusCallback.class));
		assertTrue(captor.getValue() instanceof GeneralServerFailureResponseSystemMessage);
		assertEquals(FailReason.UNEXPECTD_MESSAGE,
				((GeneralServerFailureResponseSystemMessage) captor.getValue()).getFailureReason());
	}

}
