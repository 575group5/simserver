package simserver.handlerchain.handlers.authentication;


import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import simcommon.authentication.IUserAuthentication;
import simcommon.authentication.UsernameAuthentication;
import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simserver.client.ClientConnectionManager;
import simserver.handlerchain.handlers.identification.UserIdChecker;
import simserver.history.userinfo.UserInfoKeeper;

@RunWith(PowerMockRunner.class)
@PrepareForTest(NameOnlyAuthenticationEstablisher.class)
public class NameOnlyAuthenticationEstablisherTest {

	NameOnlyAuthenticationEstablisher testObj;
	
	@Mock
	UserInfoKeeper mUserInfoKeeper;

	@Mock
	ISystemMessageSender mSender;
	
	@Mock
	UserIdChecker mUserIdChecker;
	@Mock
	private ClientConnectionManager mClientConnectionManager;
	
	@Before
	public void setup() throws Exception{
		testObj = new NameOnlyAuthenticationEstablisher(mSender, mUserIdChecker, mUserInfoKeeper,
				mClientConnectionManager, 555);
	}
	
	@Test
	public void testAuthentication_valid(){
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Collections.emptyList());
		IUserAuthentication authInfo = new UsernameAuthentication("Bob");
		
		User user = Mockito.mock(User.class);
		when(mUserInfoKeeper.addUser(any(UserInfo.class))).thenReturn(user);
		
		User result = testObj.authenticate(authInfo);
		assertEquals(user, result);
		ArgumentCaptor<UserInfo> userInfoCaptor = ArgumentCaptor.forClass(UserInfo.class);
		verify(mUserInfoKeeper, times(1)).addUser(userInfoCaptor.capture());
		assertEquals("Bob", userInfoCaptor.getValue().getName());
		verify(mClientConnectionManager, times(1)).associateUserIdWithConnection(eq(555), any(UserId.class));
	}

}
