package simserver.handlerchain.handlers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import simcommon.authentication.IUserAuthentication;
import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationId;
import simcommon.conversation.ConversationProperties;
import simcommon.entities.User;
import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.DirectoryResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simcommon.systemmessagecentral.IStatusCallback;
import simserver.history.userinfo.IConversationInfoKeeper;
import simserver.history.userinfo.IConversationVisibilityDeterminer;

public class DirectoryRequestResponderTest {

	@Test(expected = IllegalArgumentException.class)
	public void testConstructor_nullSender() {
		new DirectoryRequestResponder(null, mock(IConversationInfoKeeper.class));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructor_nullConversationInfoKeeper() {
		new DirectoryRequestResponder(mock(ISystemMessageSender.class), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHandle_nullMessage() {
		DirectoryRequestResponder subject = new DirectoryRequestResponder(mock(ISystemMessageSender.class),
				mock(IConversationInfoKeeper.class));
		subject.handle(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHandle_wrongMessageType() {
		DirectoryRequestResponder subject = new DirectoryRequestResponder(mock(ISystemMessageSender.class),
				mock(IConversationInfoKeeper.class));
		subject.handle(new AuthenticationRequestSystemMessage(mock(IUserAuthentication.class)));
	}

	@Test
	public void testHandle_sendsConversationList_matchingConversations() {
		ISystemMessageSender sender = mock(ISystemMessageSender.class);
		IConversationInfoKeeper conversationInfoKeeper = mock(IConversationInfoKeeper.class);
		DirectoryRequestResponder subject = new DirectoryRequestResponder(sender, conversationInfoKeeper);

		User userA = mock(User.class);
		User userB = mock(User.class);
		User userC = mock(User.class);
		User userD = mock(User.class);
		Conversation conversation1 = new Conversation(mock(ConversationId.class), new ConversationProperties(Arrays.asList(userA, userB), null, false));
		Conversation conversation2 = new Conversation(mock(ConversationId.class), new ConversationProperties(Arrays.asList(userA, userC, userD), "some public conversation",
				true));
		Conversation conversation3 = new Conversation(mock(ConversationId.class), new ConversationProperties(Arrays.asList(userB, userD), "some other public conversation",
				true));
		Conversation conversation4 = new Conversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(userC, userD), null, false));
		when(conversationInfoKeeper.getAvailableConversations(any(IConversationVisibilityDeterminer.class)))
				.then(new Answer<Collection<Conversation>>() {

					@Override
					public Collection<Conversation> answer(InvocationOnMock invocation) throws Throwable {
						IConversationVisibilityDeterminer conversationVisibilityDeterminer = (IConversationVisibilityDeterminer) invocation
								.getArguments()[0];
						Collection<Conversation> returnVals = new ArrayList<>();
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation1);
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation2);
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation3);
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation4);
						return returnVals;
					}

					private void addIfVisible(IConversationVisibilityDeterminer conversationVisibilityDeterminer,
							Collection<Conversation> returnVals, Conversation conversation) {
						if (conversationVisibilityDeterminer.isVisible(conversation)) {
							returnVals.add(conversation);
						}
					}
				});

		DirectoryRequestSystemMessage request = new DirectoryRequestSystemMessage(userB);

		subject.handle(request);

		ArgumentCaptor<SystemMessage> messageCaptor = ArgumentCaptor.forClass(SystemMessage.class);
		verify(sender, times(1)).sendMessage(messageCaptor.capture(), any(IStatusCallback.class));
		assertTrue(messageCaptor.getValue() instanceof DirectoryResponseSystemMessage);
		Collection<Conversation> conversations = ((DirectoryResponseSystemMessage) messageCaptor.getValue())
				.getConversations();
		assertEquals(3, conversations.size());
		assertTrue(conversations.contains(conversation1));
		assertTrue(conversations.contains(conversation2));
		assertTrue(conversations.contains(conversation3));
	}

	@Test
	public void testHandle_sendsConversationList_nonParticipatingUser() {
		ISystemMessageSender sender = mock(ISystemMessageSender.class);
		IConversationInfoKeeper conversationInfoKeeper = mock(IConversationInfoKeeper.class);
		DirectoryRequestResponder subject = new DirectoryRequestResponder(sender, conversationInfoKeeper);

		User userA = mock(User.class);
		User userB = mock(User.class);
		User userC = mock(User.class);
		User userD = mock(User.class);
		Conversation conversation1 = new Conversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(userA, userB), null, false));
		Conversation conversation2 = new Conversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(userA, userC, userD), "some public conversation", true));
		Conversation conversation3 = new Conversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(userB, userD), "some other public conversation", true));
		Conversation conversation4 = new Conversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(userC, userD), null, false));
		when(conversationInfoKeeper.getAvailableConversations(any(IConversationVisibilityDeterminer.class)))
				.then(new Answer<Collection<Conversation>>() {

					@Override
					public Collection<Conversation> answer(InvocationOnMock invocation) throws Throwable {
						IConversationVisibilityDeterminer conversationVisibilityDeterminer = (IConversationVisibilityDeterminer) invocation
								.getArguments()[0];
						Collection<Conversation> returnVals = new ArrayList<>();
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation1);
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation2);
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation3);
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation4);
						return returnVals;
					}

					private void addIfVisible(IConversationVisibilityDeterminer conversationVisibilityDeterminer,
							Collection<Conversation> returnVals, Conversation conversation) {
						if (conversationVisibilityDeterminer.isVisible(conversation)) {
							returnVals.add(conversation);
						}
					}
				});

		User userE = mock(User.class);
		DirectoryRequestSystemMessage request = new DirectoryRequestSystemMessage(userE);

		subject.handle(request);

		ArgumentCaptor<SystemMessage> messageCaptor = ArgumentCaptor.forClass(SystemMessage.class);
		verify(sender, times(1)).sendMessage(messageCaptor.capture(), any(IStatusCallback.class));
		assertTrue(messageCaptor.getValue() instanceof DirectoryResponseSystemMessage);
		Collection<Conversation> conversations = ((DirectoryResponseSystemMessage) messageCaptor.getValue())
				.getConversations();
		assertEquals(2, conversations.size());
		assertTrue(conversations.contains(conversation2));
		assertTrue(conversations.contains(conversation3));
	}

	@Test
	public void testHandle_sendsConversationList_noMatchingConversations() {
		ISystemMessageSender sender = mock(ISystemMessageSender.class);
		IConversationInfoKeeper conversationInfoKeeper = mock(IConversationInfoKeeper.class);
		DirectoryRequestResponder subject = new DirectoryRequestResponder(sender, conversationInfoKeeper);

		User userA = mock(User.class);
		User userB = mock(User.class);
		User userC = mock(User.class);
		User userD = mock(User.class);
		Conversation conversation1 = new Conversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(userA, userB), null, false));
		Conversation conversation2 = new Conversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(userA, userC, userD), "some private conversation", false));
		Conversation conversation3 = new Conversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(userB, userD), "some other private conversation", false));
		Conversation conversation4 = new Conversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(userC, userD), null, false));
		when(conversationInfoKeeper.getAvailableConversations(any(IConversationVisibilityDeterminer.class)))
				.then(new Answer<Collection<Conversation>>() {

					@Override
					public Collection<Conversation> answer(InvocationOnMock invocation) throws Throwable {
						IConversationVisibilityDeterminer conversationVisibilityDeterminer = (IConversationVisibilityDeterminer) invocation
								.getArguments()[0];
						Collection<Conversation> returnVals = new ArrayList<>();
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation1);
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation2);
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation3);
						addIfVisible(conversationVisibilityDeterminer, returnVals, conversation4);
						return returnVals;
					}

					private void addIfVisible(IConversationVisibilityDeterminer conversationVisibilityDeterminer,
							Collection<Conversation> returnVals, Conversation conversation) {
						if (conversationVisibilityDeterminer.isVisible(conversation)) {
							returnVals.add(conversation);
						}
					}
				});

		User userE = mock(User.class);
		DirectoryRequestSystemMessage request = new DirectoryRequestSystemMessage(userE);

		subject.handle(request);

		ArgumentCaptor<SystemMessage> messageCaptor = ArgumentCaptor.forClass(SystemMessage.class);
		verify(sender, times(1)).sendMessage(messageCaptor.capture(), any(IStatusCallback.class));
		assertTrue(messageCaptor.getValue() instanceof DirectoryResponseSystemMessage);
		Collection<Conversation> conversations = ((DirectoryResponseSystemMessage) messageCaptor.getValue())
				.getConversations();
		assertTrue(conversations.isEmpty());
	}
}
