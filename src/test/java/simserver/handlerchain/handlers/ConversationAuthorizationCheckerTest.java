package simserver.handlerchain.handlers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import simcommon.FailReason;
import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationId;
import simcommon.conversation.ConversationProperties;
import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.systemmessagecentral.IStatusCallback;
import simcommon.systemmessagecentral.ISystemMessageSender;

public class ConversationAuthorizationCheckerTest {

	private ISystemMessageSender mSystemMessageSender = mock(ISystemMessageSender.class);
	private ConversationAuthorizationChecker mSubject = new ConversationAuthorizationChecker(mSystemMessageSender);

	@Test(expected = IllegalArgumentException.class)
	public void testConstructor_nullSender() {
		new ConversationAuthorizationChecker(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHandle_null() {
		mSubject.handle(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHandle_wrongType() {
		mSubject.handle(new ClientDisconnectRequestSystemMessage(mock(User.class)));
	}

	@Test
	public void testHandle_authorized() {
		User user1 = new User(new UserInfo("userA"), new UserId("1"));
		User user2 = new User(new UserInfo("userB"), new UserId("2"));
		User user3 = new User(new UserInfo("userC"), new UserId("3"));
		Conversation conversation = new Conversation(new ConversationId(1l),
				new ConversationProperties(Arrays.asList(user1, user2, user3), "some group", true));
		IUserMessage message = mock(IUserMessage.class);

		boolean result = mSubject.handle(new ClientSendMessageRequestSystemMessage(user1, conversation, message));
		assertTrue(result);
		verify(mSystemMessageSender, never()).sendMessage(any(SystemMessage.class), any(IStatusCallback.class));
	}

	@Test
	public void testHandle_notAuthorized() {
		User user1 = new User(new UserInfo("userA"), new UserId("1"));
		User user2 = new User(new UserInfo("userB"), new UserId("2"));
		User user3 = new User(new UserInfo("userC"), new UserId("3"));
		Conversation conversation = new Conversation(new ConversationId(1l),
				new ConversationProperties(Arrays.asList(user1, user2, user3), "some group", true));
		IUserMessage message = mock(IUserMessage.class);

		User user4 = new User(new UserInfo("userD"), new UserId("4"));
		boolean result = mSubject.handle(new ClientSendMessageRequestSystemMessage(user4, conversation, message));
		assertFalse(result);
		ArgumentCaptor<SystemMessage> messageCaptor = ArgumentCaptor.forClass(SystemMessage.class);
		verify(mSystemMessageSender, times(1)).sendMessage(messageCaptor.capture(), any(IStatusCallback.class));
		assertTrue(messageCaptor.getValue() instanceof GeneralServerFailureResponseSystemMessage);
		assertEquals(FailReason.USER_NOT_AUTHORIZED_FOR_ACTION,
				((GeneralServerFailureResponseSystemMessage) messageCaptor.getValue()).getFailureReason());
	}
}
