package simserver.handlerchain.handlers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import simcommon.FailReason;
import simcommon.entities.User;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.IStatusCallback;
import simcommon.systemmessagecentral.ISystemMessageSender;

public class UnhandledMessageHandlerTest {

	private ISystemMessageSender mMessageSender = mock(ISystemMessageSender.class);
	private UnhandledMessageHandler mSubject = new UnhandledMessageHandler(mMessageSender);

	@Test(expected = IllegalArgumentException.class)
	public void testConstructor_nullParam() {
		new UnhandledMessageHandler(null);
	}

	@Test
	public void testHandle() {
		mSubject.handle(new ClientDisconnectRequestSystemMessage(mock(User.class)));
		ArgumentCaptor<SystemMessage> captor = ArgumentCaptor.forClass(SystemMessage.class);
		verify(mMessageSender, times(1)).sendMessage(captor.capture(), any(IStatusCallback.class));
		assertTrue(captor.getValue() instanceof GeneralServerFailureResponseSystemMessage);
		assertEquals(FailReason.UNRECOGNIZED_REQUEST,
				((GeneralServerFailureResponseSystemMessage) captor.getValue()).getFailureReason());
	}

}
