package simserver.handlerchain.handlers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationId;
import simcommon.conversation.ConversationProperties;
import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.GeneralServerSuccessResponseSystemMessage;
import simcommon.messages.systemMessages.ServerSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.UserOrigin;
import simcommon.systemmessagecentral.IStatusCallback;
import simcommon.systemmessagecentral.ISystemMessageSender;
import simserver.client.ClientConnectionManager;
import simserver.client.ClientHandler;

public class MessageDistributerTest {

	private ClientConnectionManager mClientConnectionManager = mock(ClientConnectionManager.class);
	private MessageDistributer mSubject = new MessageDistributer(mClientConnectionManager);

	@Test(expected = IllegalArgumentException.class)
	public void testConstructor_null() {
		new MessageDistributer(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHandle_null() {
		mSubject.handle(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHandle_wrongMessageType() {
		mSubject.handle(new ClientDisconnectRequestSystemMessage(mock(User.class)));
	}

	@Test
	public void testHandle_sendsToParticipants() {
		User user1 = new User(new UserInfo("userA"), new UserId("1"));
		User user2 = new User(new UserInfo("userB"), new UserId("2"));
		User user3 = new User(new UserInfo("userC"), new UserId("3"));
		Conversation conversation = new Conversation(new ConversationId(1l),
				new ConversationProperties(Arrays.asList(user1, user2, user3), "some group", true));
		IUserMessage message = mock(IUserMessage.class);
		ClientSendMessageRequestSystemMessage clientSendMessageRequestSystemMessage = new ClientSendMessageRequestSystemMessage(
				user2, conversation, message);

		ISystemMessageSender messageSender1 = mock(ISystemMessageSender.class);
		ClientHandler handler1 = mock(ClientHandler.class);
		when(handler1.getMessageSender()).thenReturn(messageSender1);

		ISystemMessageSender messageSender2 = mock(ISystemMessageSender.class);
		ClientHandler handler2 = mock(ClientHandler.class);
		when(handler2.getMessageSender()).thenReturn(messageSender2);

		ISystemMessageSender messageSender3 = mock(ISystemMessageSender.class);
		ClientHandler handler3 = mock(ClientHandler.class);
		when(handler3.getMessageSender()).thenReturn(messageSender3);

		when(mClientConnectionManager.getClientHandlerForUser(eq(new UserId("1")))).thenReturn(handler1);
		when(mClientConnectionManager.getClientHandlerForUser(eq(new UserId("2")))).thenReturn(handler2);
		when(mClientConnectionManager.getClientHandlerForUser(eq(new UserId("3")))).thenReturn(handler3);

		boolean result = mSubject.handle(clientSendMessageRequestSystemMessage);
		assertTrue(result);
		ArgumentCaptor<SystemMessage> messageCaptor = ArgumentCaptor.forClass(SystemMessage.class);

		verify(messageSender1, times(1)).sendMessage(messageCaptor.capture(), any(IStatusCallback.class));
		assertTrue(messageCaptor.getValue() instanceof ServerSendMessageRequestSystemMessage);
		ServerSendMessageRequestSystemMessage serverSend1 = (ServerSendMessageRequestSystemMessage) messageCaptor
				.getValue();
		assertEquals(new UserOrigin(user2), serverSend1.getSource());
		assertEquals(conversation, serverSend1.getConversation());
		assertEquals(message, serverSend1.getMessage());

		verify(messageSender3, times(1)).sendMessage(messageCaptor.capture(), any(IStatusCallback.class));
		assertTrue(messageCaptor.getValue() instanceof ServerSendMessageRequestSystemMessage);
		ServerSendMessageRequestSystemMessage serverSend3 = (ServerSendMessageRequestSystemMessage) messageCaptor
				.getValue();
		assertEquals(new UserOrigin(user2), serverSend3.getSource());
		assertEquals(conversation, serverSend3.getConversation());
		assertEquals(message, serverSend3.getMessage());

		verify(messageSender2, times(1)).sendMessage(messageCaptor.capture(), any(IStatusCallback.class));
		assertTrue(messageCaptor.getValue() instanceof GeneralServerSuccessResponseSystemMessage);
	}
}
