package simserver.handlerchain.handlers;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.PrintStream;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import simcommon.messages.systemMessages.SystemMessage;

public class SystemMessageLoggerTest {

	@Test
	public void testHandle_printsMessageClass() {
		PrintStream mockPrintStream = mock(PrintStream.class);
		SystemMessageLogger systemMessageLogger = new SystemMessageLogger() {

			@Override
			public PrintStream getOutputStream() {
				return mockPrintStream;
			}
		};
		SystemMessage systemMessage = mock(SystemMessage.class);
		systemMessageLogger.handle(systemMessage);

		ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);
		verify(mockPrintStream, times(1)).println(stringCaptor.capture());
		assertTrue(stringCaptor.getValue().contains(systemMessage.getClass().toString()));
	}

}
