package simserver.handlerchain;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class HandlerChainBuilderTest {

	@Test(expected = IllegalArgumentException.class)
	public void testAddCheck_invalid() {
		new HandlerChainBuilder().forCheckInOrder(null);
	}

	@Test(expected = IllegalStateException.class)
	public void testAddCheck_addTwice() {
		new HandlerChainBuilder().forCheckInOrder(mock(IMessageCheck.class)).forCheckInOrder(mock(IMessageCheck.class));
	}

	@Test(expected = IllegalStateException.class)
	public void testAddCheck_addDefaultWithoutHandler() {
		new HandlerChainBuilder().forCheckInOrder(mock(IMessageCheck.class)).forDefault();
	}

	@Test(expected = IllegalStateException.class)
	public void testAddCheck_alreadyBuilt() {
		HandlerChainBuilder handlerChainBuilder = new HandlerChainBuilder();
		handlerChainBuilder.build();
		handlerChainBuilder.forCheckInOrder(mock(IMessageCheck.class));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddHandler_null() {
		new HandlerChainBuilder().forCheckInOrder(mock(IMessageCheck.class)).addHandlerInOrder(null);
	}

	@Test(expected = IllegalStateException.class)
	public void testAddHandler_withoutCheck() {
		new HandlerChainBuilder().addHandlerInOrder(mock(IMessageHandlerLogic.class));
	}

	@Test(expected = IllegalStateException.class)
	public void testAddHandler_alreadyBuilt() {
		HandlerChainBuilder handlerChainBuilder = new HandlerChainBuilder().forCheckInOrder(mock(IMessageCheck.class))
				.addHandlerInOrder(mock(IMessageHandlerLogic.class));
		handlerChainBuilder.build();
		handlerChainBuilder.addHandlerInOrder(mock(IMessageHandlerLogic.class));
	}

	@Test(expected = IllegalStateException.class)
	public void testBuild_addCheck_withoutHandler() {
		new HandlerChainBuilder().forCheckInOrder(mock(IMessageCheck.class)).build();
	}

	@Test
	public void testBuild_minimal() {
		assertNotNull(new HandlerChainBuilder().build());
	}

	@Test
	public void testBuild_defaultOnly() {
		assertNotNull(
				new HandlerChainBuilder().forDefault().addHandlerInOrder(mock(IMessageHandlerLogic.class)).build());
	}

	@Test
	public void testBuild_oneCheck() {
		assertNotNull(new HandlerChainBuilder().forCheckInOrder(mock(IMessageCheck.class))
				.addHandlerInOrder(mock(IMessageHandlerLogic.class)).addHandlerInOrder(mock(IMessageHandlerLogic.class))
				.build());
	}

	@Test(expected = IllegalStateException.class)
	public void testBuild_alreadyBuilt() {
		IMessageCheck check1 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerA = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic handlerB = mock(IMessageHandlerLogic.class);

		HandlerChainBuilder handlerChainBuilder = new HandlerChainBuilder().forCheckInOrder(check1)
				.addHandlerInOrder(handlerA).addHandlerInOrder(handlerB);
		handlerChainBuilder.build();
		handlerChainBuilder.build();
	}

	@Test(expected = IllegalStateException.class)
	public void testGetContents_alreadyBuilt() {
		IMessageCheck check1 = mock(IMessageCheck.class);
		IMessageHandlerLogic handlerA = mock(IMessageHandlerLogic.class);
		IMessageHandlerLogic handlerB = mock(IMessageHandlerLogic.class);

		HandlerChainBuilder handlerChainBuilder = new HandlerChainBuilder().forCheckInOrder(check1)
				.addHandlerInOrder(handlerA).addHandlerInOrder(handlerB);
		handlerChainBuilder.build();

		handlerChainBuilder.getContents();
	}
}
