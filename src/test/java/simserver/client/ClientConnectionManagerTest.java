package simserver.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import simcommon.entities.UserId;
import simserver.handlerchain.HandlerChain;

@RunWith(MockitoJUnitRunner.class)
public class ClientConnectionManagerTest {

	private ClientConnectionManager testObj = new ClientConnectionManager();
	
	@Test
	public void testConnectionIdGeneration(){
		assertEquals(0, testObj.generateConnectionId());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddClientHandler_invalidParam_nullClientHandler() {
		testObj.addClientHandler(1, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddClientHandler_invalidParam_existingConnectionId() {
		ClientHandler clientHandler1 = Mockito.mock(ClientHandler.class);
		ClientHandler clientHandler2 = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(1, clientHandler1);
		testObj.addClientHandler(1, clientHandler2);
	}
	
	@Test
	public void testAddClientHandler_started() {
		ClientHandler clientHandler = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(1, clientHandler);
		Mockito.verify(clientHandler, Mockito.times(1)).start();
		Mockito.verify(clientHandler, Mockito.never()).stop();
		Mockito.verify(clientHandler, Mockito.never()).attachHandlerChain(Mockito.any(HandlerChain.class));
		Mockito.verify(clientHandler, Mockito.never()).getMessageSender();
	}

	@Test
	public void testAddClientHandler_canRetrieve() {
		ClientHandler clientHandler = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(1, clientHandler);
		assertEquals(clientHandler, testObj.getClientHandler(1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoveClientHandler_empty() {
		testObj.removeClientHandler(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoveClientHandler_unusedId() {
		ClientHandler clientHandler = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(3, clientHandler);
		testObj.removeClientHandler(0);
	}

	@Test
	public void testRemoveClientHandler_containsHandler() {
		ClientHandler clientHandler = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(3, clientHandler);

		assertEquals(clientHandler, testObj.getClientHandler(3));

		testObj.removeClientHandler(3);

		assertNull(testObj.getClientHandler(3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAssociateUserIdWithConnection_invalidParam_nullUserId() {
		ClientHandler clientHandler = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(3, clientHandler);

		testObj.associateUserIdWithConnection(3, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAssociateUserIdWithConnection_invalidParam_unusedConnectionId() {
		ClientHandler clientHandler = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(3, clientHandler);

		testObj.associateUserIdWithConnection(4, Mockito.mock(UserId.class));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAssociateUserIdWithConnection_invalidParam_alreadyAssociatedUserId() {
		ClientHandler clientHandler1 = Mockito.mock(ClientHandler.class);
		ClientHandler clientHandler2 = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(3, clientHandler1);
		testObj.addClientHandler(4, clientHandler2);

		UserId userId = Mockito.mock(UserId.class);
		testObj.associateUserIdWithConnection(3, userId);
		testObj.associateUserIdWithConnection(4, userId);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAssociateUserIdWithConnection_invalidParam_alreadyUsedConnectionId() {
		ClientHandler clientHandler1 = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(3, clientHandler1);

		UserId userIdA = Mockito.mock(UserId.class);
		UserId userIdB = Mockito.mock(UserId.class);
		testObj.associateUserIdWithConnection(3, userIdA);
		testObj.associateUserIdWithConnection(3, userIdB);
	}

	@Test
	public void testAssociateUserIdWithConnection_allowsLookup() {
		ClientHandler clientHandler1 = Mockito.mock(ClientHandler.class);
		ClientHandler clientHandler2 = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(3, clientHandler1);
		testObj.addClientHandler(4, clientHandler2);

		UserId userId = Mockito.mock(UserId.class);
		testObj.associateUserIdWithConnection(4, userId);

		assertEquals(clientHandler2, testObj.getClientHandlerForUser(userId));
	}

	@Test
	public void testGetClientHandlerForUser_userNotPresent() {
		ClientHandler clientHandler1 = Mockito.mock(ClientHandler.class);
		ClientHandler clientHandler2 = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(3, clientHandler1);
		testObj.addClientHandler(4, clientHandler2);

		UserId userId = Mockito.mock(UserId.class);
		assertNull(testObj.getClientHandlerForUser(userId));
	}

	@Test
	public void testGetClientHandlerForUser_clientConnectionRemoved() {
		ClientHandler clientHandler1 = Mockito.mock(ClientHandler.class);
		ClientHandler clientHandler2 = Mockito.mock(ClientHandler.class);
		testObj.addClientHandler(3, clientHandler1);
		testObj.addClientHandler(4, clientHandler2);

		UserId userId = Mockito.mock(UserId.class);
		testObj.associateUserIdWithConnection(4, userId);

		testObj.removeClientHandler(4);
		assertNull(testObj.getClientHandlerForUser(userId));
	}
}
