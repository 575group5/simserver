package simserver.client;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.ServerSocket;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import simcommon.entities.StandardServerInfo;
import simcommon.serialization.IMessageDeserializer;
import simcommon.serialization.IMessageSerializer;
import simserver.client.ClientAcceptor.AcceptClientException;
import simserver.client.SocketClientAcceptor.CheckForClientsRunnable;
@RunWith(PowerMockRunner.class)
@PrepareForTest(SocketClientAcceptor.class)
public class SocketClientAcceptorTest {
	@Mock
	private IMessageSerializer mMockSerializer;
	@Mock
	private IMessageDeserializer mMockDeserializer;
	@InjectMocks
	private SocketClientAcceptor testObj;
	@Mock
	private ServerSocket mServerSocket;
	private int port;
	private StandardServerInfo serverInfo;

	@Before
	public void setup() throws Exception{
		port = 8080;
		Whitebox.setInternalState(testObj, "mServerSocket", mServerSocket);
		PowerMockito.whenNew(ServerSocket.class).withArguments(port).thenReturn(mServerSocket);
		CheckForClientsRunnable mockRunnable = Mockito.mock(CheckForClientsRunnable.class);
		PowerMockito.whenNew(CheckForClientsRunnable.class).withNoArguments().thenReturn(mockRunnable);
	}
	
	//@Test
	public void testAccepting() throws Exception{
		givenPort();
		whenAcceptingClients();
		thenServerSocketEstablished();
		thenClientsRunnableEstablished();
	}
	
	//@Test
	public void testCloseSocket() throws IOException{
		whenStopAcceptingClients();
		thenSocketIsClosed();
	}
	
	private void thenSocketIsClosed() throws IOException {
		verify(mServerSocket,times(1)).close();
	}

	private void whenStopAcceptingClients() {
		testObj.stopAcceptingClients();
	}

	private void thenClientsRunnableEstablished() throws Exception {
		PowerMockito.verifyNew(CheckForClientsRunnable.class).withNoArguments();
	}
	
	private void thenServerSocketEstablished() throws Exception {
		PowerMockito.verifyNew(ServerSocket.class).withArguments(port);
	}

	private void whenAcceptingClients() throws AcceptClientException {
		testObj.startAcceptingClients(serverInfo);
	}

	private void givenPort() {
		serverInfo = Mockito.mock(StandardServerInfo.class);
		when(serverInfo.getPort()).thenReturn(port);
	}
}
