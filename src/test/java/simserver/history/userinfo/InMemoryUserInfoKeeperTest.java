package simserver.history.userinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.Test;

import simcommon.entities.User;
import simcommon.entities.UserInfo;

public class InMemoryUserInfoKeeperTest {
	
	private InMemoryUserInfoKeeper mSubject = new InMemoryUserInfoKeeper();
	
	@Test
	public void testAddUser_returnValue() {
		UserInfo userInfo = new UserInfo("user50");
		User user = mSubject.addUser(userInfo);
		assertNotNull(user);
		assertEquals("user50", user.getUserInfo().getName());
	}
	
	@Test
	public void testGetAllUsers() {
		mSubject.addUser(new UserInfo("user50"));
		mSubject.addUser(new UserInfo("user60"));
		mSubject.addUser(new UserInfo("anon"));
		mSubject.addUser(new UserInfo("userX"));

		Collection<User> all = mSubject.getAllUsers();
		assertEquals(4, all.size());
	}

}
