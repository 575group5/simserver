package simserver.history.userinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationId;
import simcommon.conversation.ConversationProperties;
import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;

public class InMemoryConversationInfoKeeperTest {

	private UserInfoKeeper mUserInfoKeeper = mock(UserInfoKeeper.class);

	@Test(expected = IllegalArgumentException.class)
	public void testConstructor_invalidValues_nullUserInfo() {
		new InMemoryConversationInfoKeeper(null);
	}

	@Test
	public void testConstructor_generateFromExisting() {
		List<User> users = new ArrayList<>();
		users.add(new User(new UserInfo("user1"), new UserId("1")));
		users.add(new User(new UserInfo("userA"), new UserId("a")));
		users.add(new User(new UserInfo("userX"), new UserId("2fg")));
		when(mUserInfoKeeper.getAllUsers()).thenReturn(users);

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		Collection<Conversation> allConversations = getAllConversations(subject);
		assertEquals(3, allConversations.size());
		assertAtLeastOneMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getParticipants().contains(users.get(0))
						&& conversation.getParticipants().contains(users.get(1));
			}
		});
		assertAtLeastOneMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getParticipants().contains(users.get(0))
						&& conversation.getParticipants().contains(users.get(2));
			}
		});
		assertAtLeastOneMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getParticipants().contains(users.get(1))
						&& conversation.getParticipants().contains(users.get(2));
			}
		});
		assertAllMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getProperties().getName() == null;
			}
		});
	}
	
	@Test
	public void testAddUser_someAlready() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);
		
		User newUser = new User(new UserInfo("userB"), new UserId("bg"));
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3, newUser));
		subject.onUserAdded(newUser);

		Collection<Conversation> allConversations = getAllConversations(subject);
		assertEquals(6, allConversations.size());
		assertAtLeastOneMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getParticipants().contains(user1)
						&& conversation.getParticipants().contains(newUser);
			}
		});
		assertAtLeastOneMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getParticipants().contains(user2)
						&& conversation.getParticipants().contains(newUser);
			}
		});
		assertAtLeastOneMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getParticipants().contains(user3)
						&& conversation.getParticipants().contains(newUser);
			}
		});
		assertAllMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getProperties().getName() == null;
			}
		});
	}
	
	@Test
	public void testAddUser_firstUser() {
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Collections.emptyList());

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);
		
		User newUser = new User(new UserInfo("userB"), new UserId("bg"));
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(newUser));
		subject.onUserAdded(newUser);

		Collection<Conversation> allConversations = getAllConversations(subject);
		assertEquals(0, allConversations.size());
	}
	
	
	@Test
	public void testAddUser_firstAndSecondUser() {
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Collections.emptyList());

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);
		
		User userA = new User(new UserInfo("userA"), new UserId("asdfg"));
		User userB = new User(new UserInfo("userB"), new UserId("hjkl;"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(userA));
		subject.onUserAdded(userA);

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(userA, userB));
		subject.onUserAdded(userB);

		Collection<Conversation> allConversations = getAllConversations(subject);
		assertEquals(1, allConversations.size());
		assertAtLeastOneMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getParticipants().contains(userA) && conversation.getParticipants().contains(userB);
			}
		});
		assertAllMatching(allConversations, new ConversationAssert() {
			@Override
			public boolean match(Conversation conversation) {
				return conversation.getProperties().getName() == null;
			}
		});
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddUser_null() {
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Collections.emptyList());
		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);
		subject.onUserAdded(null);
	}

	@Test
	public void testRemoveUser_someAlready() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));
		User user4 = new User(new UserInfo("userZ"), new UserId("2fi"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3, user4));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));
		subject.onUserRemoved(user4);

		assertEquals(3, getAllConversations(subject).size());

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2));
		subject.onUserRemoved(user3);

		assertEquals(1, getAllConversations(subject).size());

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1));
		subject.onUserRemoved(user2);

		assertEquals(0, getAllConversations(subject).size());

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Collections.emptyList());
		subject.onUserRemoved(user1);

		assertEquals(0, getAllConversations(subject).size());
	}

	@Test(expected = IllegalStateException.class)
	public void testRemoveUser_noExistingUsers() {
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Collections.emptyList());
		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);
		subject.onUserRemoved(new User(new UserInfo("user1"), new UserId("1")));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoveUser_null() {
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Collections.emptyList());
		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);
		subject.onUserRemoved(null);
	}

	@Test
	public void testGetConversation_validId() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		Collection<Conversation> allConversations = getAllConversations(subject);

		for (Conversation conversation : allConversations) {
			ConversationId id = conversation.getConversationId();
			Conversation retrieved = subject.getConversation(id);
			assertEquals(conversation, retrieved);
			assertEquals(id, retrieved.getConversationId());

		}
	}

	@Test
	public void testGetConversation_invalidId() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		assertNull(subject.getConversation(mock(ConversationId.class)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetConversation_null() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		subject.getConversation(null);
	}

	@Test
	public void testAddConversation_hasExisting() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		ConversationProperties newConversationProperties = new ConversationProperties(
				Arrays.asList(user1, user2, user3), "everybody", false);
		ConversationId newId = subject.addConversation(newConversationProperties);

		assertEquals(4, getAllConversations(subject).size());

		Conversation conversation = subject.getConversation(newId);
		assertEquals(newConversationProperties, conversation.getProperties());
	}

	@Test
	public void testAddConversation_noneExisting() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		ConversationProperties newConversationProperties = new ConversationProperties(Arrays.asList(user1),
				"a public group", true);
		ConversationId newId = subject.addConversation(newConversationProperties);

		assertEquals(1, getAllConversations(subject).size());

		Conversation conversation = subject.getConversation(newId);
		assertEquals(newConversationProperties, conversation.getProperties());
	}

	@Test
	public void testRemoveConversation_addedConversation() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		ConversationProperties newConversationProperties = new ConversationProperties(
				Arrays.asList(user1, user2, user3), "everybody", false);
		ConversationId newId = subject.addConversation(newConversationProperties);

		boolean success = subject.removeConversation(newId);
		assertTrue(success);
		assertNull(subject.getConversation(newId));
		assertEquals(3, getAllConversations(subject).size());
	}

	@Test
	public void testRemoveConversation_authGeneratedConversation() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		ConversationId someConversationId = getAllConversations(subject).iterator().next().getConversationId();

		boolean success = subject.removeConversation(someConversationId);
		assertFalse(success);
		assertNotNull(subject.getConversation(someConversationId));
		assertEquals(3, getAllConversations(subject).size());
	}

	@Test
	public void testRemoveConversation_invalidConversationId() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		boolean success = subject.removeConversation(mock(ConversationId.class));
		assertFalse(success);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoveConversation_null() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		subject.removeConversation(null);
	}

	@Test
	public void testUpdateConversation_wasAdded_participantsStaySame() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		ConversationProperties firstConversationProperties = new ConversationProperties(Arrays.asList(user1, user2),
				"new private group", false);
		ConversationId newId = subject.addConversation(firstConversationProperties);

		assertEquals(4, getAllConversations(subject).size());

		ConversationProperties secondConversationProperties = new ConversationProperties(Arrays.asList(user1, user2),
				"new public group", true);
		subject.updateConversation(newId, secondConversationProperties);

		assertEquals(4, getAllConversations(subject).size());

		Conversation conversation = subject.getConversation(newId);
		assertEquals(secondConversationProperties, conversation.getProperties());
	}

	@Test
	public void testUpdateConversation_wasAdded_participantsChange() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		ConversationProperties firstConversationProperties = new ConversationProperties(Arrays.asList(user1, user2),
				"new group", false);
		ConversationId newId = subject.addConversation(firstConversationProperties);

		assertEquals(4, getAllConversations(subject).size());

		ConversationProperties secondConversationProperties = new ConversationProperties(
				Arrays.asList(user1, user2, user3), "new group", false);
		subject.updateConversation(newId, secondConversationProperties);

		assertEquals(4, getAllConversations(subject).size());

		Conversation conversation = subject.getConversation(newId);
		assertEquals(secondConversationProperties, conversation.getProperties());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUpdateConversation_autogenerated() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		ConversationId someConversationId = getAllConversations(subject).iterator().next().getConversationId();

		subject.updateConversation(someConversationId,
				new ConversationProperties(Arrays.asList(user1, user2, user3), "new group", false));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUpdateConversation_invalidId() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		subject.updateConversation(mock(ConversationId.class),
				new ConversationProperties(Arrays.asList(user1, user2, user3), "new group", false));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUpdateConversation_nullId() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));
		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		subject.updateConversation(null,
				new ConversationProperties(Arrays.asList(user1, user2, user3), "new group", false));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUpdateConversation_nullProperties() {
		User user1 = new User(new UserInfo("user1"), new UserId("1"));
		User user2 = new User(new UserInfo("userA"), new UserId("a"));
		User user3 = new User(new UserInfo("userX"), new UserId("2fg"));

		when(mUserInfoKeeper.getAllUsers()).thenReturn(Arrays.asList(user1, user2, user3));

		InMemoryConversationInfoKeeper subject = new InMemoryConversationInfoKeeper(
				mUserInfoKeeper);

		ConversationProperties conversationProperties = new ConversationProperties(Arrays.asList(user1, user2),
				"new private group", false);
		ConversationId newId = subject.addConversation(conversationProperties);

		assertEquals(4, getAllConversations(subject).size());

		subject.updateConversation(newId, null);
	}

	private Collection<Conversation> getAllConversations(InMemoryConversationInfoKeeper subject) {
		return subject.getAvailableConversations(new IConversationVisibilityDeterminer() {
			@Override
			public boolean isVisible(Conversation conversation) {
				return true;
			}
		});
	}

	private void assertAtLeastOneMatching(Collection<Conversation> conversations,
			ConversationAssert conversationAssert) {
		for (Conversation conversation : conversations) {
			if (conversationAssert.match(conversation)) {
				return;
			}
		}
		assertTrue("no conversation met criteria", false);
	}

	private void assertAllMatching(Collection<Conversation> conversations, ConversationAssert conversationAssert) {
		for (Conversation conversation : conversations) {
			if (!conversationAssert.match(conversation)) {
				assertTrue("no conversation met criteria", false);
			}
		}

	}

	private interface ConversationAssert {
		boolean match(Conversation conversation);
	}

}
