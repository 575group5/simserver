package drivers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import simcommon.entities.StandardServerInfo;
import simcommon.serialization.IMessageDeserializer;
import simcommon.serialization.IMessageSerializer;
import simserver.client.ClientAcceptor.AcceptClientException;
import simserver.client.IClientAcceptedCallback;
import simserver.client.SocketClientAcceptor;
import simserver.server.ServerLauncher;
import simserver.server.SimServerLauncher;

@RunWith(MockitoJUnitRunner.class)
public class StartServerDriver {
	
	IMessageSerializer mMockSerializer = Mockito.mock(IMessageSerializer.class);
	IMessageDeserializer mMockDeserializer = Mockito.mock(IMessageDeserializer.class);
	IClientAcceptedCallback mMockCallback = Mockito.mock(IClientAcceptedCallback.class);
	
	@Test
	public void startServer() throws AcceptClientException{
		
		ServerLauncher launcher = new SimServerLauncher();
		launcher.launchServer();
	}

}
