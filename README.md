# SimServer

SimServer is a Java application that represents the server component of the SiMessenger project. The SimServer is intended to function with many running instances of the SimClient component, to complete the client/server architecture in which SiMessenger is comprised.

SimServer consists of a command line interface intended only for activity and exception viewing. The intance of the SimServer shall be running on the same local network as its connected clients in order to achieve complete functionality of the clients.

## Installation

The project utilizes Apache Maven(https://maven.apache.org/) for dependency management and to build an executable jar.
For maven integration with eclipse a minimum version of Mars 4.5.1 is recommended.
After cloning the repository and importing the project into eclipse executing the following command will produce a jar:

mvn clean build

If eclipse is being used jars can also be produced by utilizing the interface and doing the following:
Right click the project -> Run As -> Maven Install

Two jars will be produced in the target directory of the project:

simserver-1.0.0-jar-with-dependencies.jar - executable and will launch the server
simserver-1.0.0.jar - not executable and will contain the project source code


## History

1.0 First release

## Credits
Ken Balogh, Dan Ford, Greg Warchol, Lindsey Young